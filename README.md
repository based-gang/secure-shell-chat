# Secure Shell Chat

Secure Shell Chat (SSHC) is a chat client that uses the SSH protocol. It's being developed mainly by two people: Kluzey and Lolly.

## TODO

- set up basic backend & frontend

## Compiling

use `gcc` or `clang`

example `clang -lssh`

## Using GIT

`git add (file)` to ready a file for commiting

`git commit (file) -m "(messge)"` to commit file **locally**

`git status` to check status

`git push` to push changes to repo

`git pull` to update local repo
## ssh key 
ssh-keygen -t rsa
