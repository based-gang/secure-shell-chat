SRC = src/backend.c

CC = gcc
FLAGS = -lssh
DEST = /bin/
NAME = sshc

all: sshc

sshc:
	$(CC) $(SRC) -o $(NAME) $(FLAGS)

install: all
	mv $(NAME) $(DEST)

uninstall:
	rm -f /bin/$(NAME)

.PHONY: install uninstall all
